# Escape Pod

This is a set of components for backing up various public-cloud services.

You can back up your Google Photos account to GCP or AWS, and download
a local copy of the archive.

## How?

The Google Photos Scraper is a rolling survey of what's in Google
Photos. When it finds something new, it makes a copy of it. These
appear to be less than full-quality.

The Google Photos Dumper listens to an email address for the
notification that a Google Takeout archive is available. It downloads
the archive, uncompresses, & reassembles it.

Both of these tools alert whenever they haven't run "lately", or
whenever a change is detected in the availability of Google's
interface.

Both of these tools write to a well-known location. An AWS Exporter
(and potentially an Azure or other Exporters) copies to AWS storage.

A Downloader makes it easy to take a local copy of the archive.


## Yeah, but . . .

**How slow will it be / how much will it cost?** Embed metrics from the start.

**What if I can't process the quarterly Takeout archive! I'm left with
low-quality photos!** Perhaps a "Takeout Scheduler" that can request
multiple archive schedules, interleaved? (E.g. Job A1, Job B1, Job A2,
Job B2 . . .)

**Google's just gonna change the API** Well, that's why each component
complains loudly if it stop working. This includes if it haven't seen
any new work to do "lately".


# Risks

**TODO ASAP determine how much data, and of what importance, is lost**
in each of Scraper and Archivist's downloads.

_(Although we're not going to **quit** simply because we lose
metadata. It merely adjusts how we feel about it.)_


# Design

## Alerting

Of course all errors always

## Scraper

- a scraper
  - authz to
    - Google Photos
    - job queue
    - image sink
  - small Python script or Go binary
    - Get image list from Google Photos
    - Compare list to image sink
      - for image in list if not in sink
        - write job to queue
  - stateless
  - runs periodically
  - alerts
    - if images / week < 1

- a job queue
  - alerts
    - if depth is > 0 and slope is flat
    - if transactions / week < 2

- a job minder
  - authz to
    - job queue
  - triggers dead jobs
  - reaps completed jobs

- a job runner
  - a Python function
    - Get a job from the queue
    - Run the job
      - Get the image from Google Photos
      - Check the image for correctness / completeness
      - Write the image to the image sink
    - Update job status in queue
  - authz to
    - Google Photos
    - job queue
    - image sink

- an image sink
  - alerts
    - if size delta / week <= 0

## Runtime
  - a Python script or a Go binary
    - in a container?
    - serverless?
      - 

## Monitoring
- cost
  - sleeping
  - storage
  - bandwidth
  - active per-image
  - month to date

## Build

CICD obv.

### Docker image build
  - Push directly to AWS / GCP

### Go binary build

### Python dependency management

### Infra

- Terraform for AWS & GCP


## Data

Somehow you have to do Google authn.

CICD needs deploy creds for AWS & GCP authn / authz.


## Developing

Local dev env should be simple, replication in prod-like conditions should be easy.

See [Dev Journal](https://gitlab.com/christopher-demarco/escape-pod/-/wikis/dev-journal).

## Operation

Cloud ops must be observable.

Metrics first--if it matters it's measured, and only then addressed.
